"""""""""""""""""""""Setting of xfzhang""""""""""""""""""""""""""""
" ==== Reference: https://github.com/ruchee/vimrc;
set nocompatible
filetype off
syntax enable
syntax on
filetype indent on
filetype plugin on

" File Type
" au BufRead,BufNewFile *.log	setlocal ft=conf
"
" Normal Setting
set shiftwidth=4
set tabstop=4
set expandtab
set backspace=2
set autoindent
set ai!
set smartindent
set nu
set ruler
set incsearch
set wrap
set hidden
set autochdir
set foldmethod=indent
set foldlevel=100
set laststatus=2
set nobackup
set noswapfile
set t_Co=256
set background=dark
set signcolumn=number

" Encoding
set fencs=utf-8
set encoding=utf-8
set fileencodings=utf-8,gbk,cp936,latin-1,shift-jis
set fileformat=unix
set fileformats=unix,dos,mac
source $VIMRUNTIME/delmenu.vim
source $VIMRUNTIME/menu.vim

" GUI
let g:cpp_class_scope_highlight = 1
let g:cpp_experimental_template_highlight = 1
let g:python_highlight_all = 1

" hot key
nnoremap nw <C-W><C-W>

" Setting for fortran
au FileType fortran setlocal tw=0
if (expand("%:e") ==? "f90" || expand("%:e") ==? "f08")
  let fortran_free_source = 1
  set shiftwidth=2
  set tabstop=2
  set foldmethod=syntax
elseif (expand("%:e") ==? "f" || expand("%:e") ==? "f77")
  let fortran_fixed_source = 1
  set shiftwidth=2
  set tabstop=2
endif
" let fortran_more_precise = 1
let fortran_do_enddo = 1
let fortran_have_tabs = 1
let fortran_fold = 1
let fortran_fold_conditionals = 1

" ==== Plugins ====
""" == Vim-plug
call plug#begin('~/.vim/plugged')
Plug 'dense-analysis/ale'
Plug 'scrooloose/nerdtree', {'on': 'NERDTreeToggle'}
Plug 'scrooloose/nerdcommenter'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'Yggdroot/indentLine'
Plug 'Yggdroot/LeaderF', {'do': ':LeaderfInstallCExtension'}
Plug 'mhinz/vim-signify'
Plug 'octol/vim-cpp-enhanced-highlight'
Plug 'vim-scripts/a.vim'
Plug 'sheerun/vim-polyglot'
Plug 'ludovicchabant/vim-gutentags'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'luochen1990/rainbow'
Plug 'tomasr/molokai'
call plug#end()
"
"" == NERDTree
noremap <F12> :NERDTreeToggle<CR>
"
""" == ALE
let g:ale_disable_lsp = 1
let g:ale_sign_column_always = 1
let g:ale_linters_explicit = 1
let g:ale_echo_msg_format = '[%linter%] %code: %%s'
let g:airline#extensions#ale#enabled = 1
let g:ale_lint_on_text_changed = 'normal'
let g:ale_lint_on_insert_leave = 1
let g:ale_linters = {
  \ 'c': ['gcc', 'cppcheck'],
  \ 'cpp': ['gcc', 'cppcheck'],
  \ 'python': ['flake8'],
  \ 'fortran': ['gcc']
  \}
let g:ale_c_gcc_options = '-Wall -std=c11'
let g:ale_cpp_gcc_options = '-Wall -std=c++14'
let g:ale_fortran_gcc_options = '-ffree-line-length-none'
nnoremap <silent> <C-k> <Plug>(ale_previous_wrap)
nnoremap <silent> <C-j> <Plug>(ale_next_wrap)
"
""" == Vim-signify
set updatetime=300
"
""" == Echodoc
set noshowmode
let g:echodoc#enable_at_startup = 1
"
""" == Gutentags
let g:gutentags_project_root = ['.root', '.svn', '.git', '.hg', '.project']
let g:gutentags_ctags_tagfile = '.tags'
let s:vim_tags = expand('~/.cache/tags')
let g:gutentags_cache_dir = s:vim_tags
let g:gutentags_ctags_extra_args = ['--fields=+niazS', '--extra=+q']
let g:gutentags_ctags_extra_args += ['--c++-kinds=+px']
let g:gutentags_ctags_extra_args += ['--c-kinds=+px']
if !isdirectory(s:vim_tags)
  silent! call mkdir(s:vim_tags, 'p')
endif
"
""" === Coc.nvim
" use tab for trigger completion with characters ahead & navigate.
inoremap <silent><expr> <TAB>
  \ pumvisible() ? "\<C-n>" :
  \ <SID>check_back_space() ? "\<TAB>" :
  \ coc#refresh()
function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"
" use <c-space> to trigger completion
inoremap <silent><expr> <C-@> coc#refresh()
" Make <CR> auto-select the first completion item and notify coc.nvim to
" format on enter, <cr> could be remapped by other vim plugin
inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm()
  \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"
"
""" === LeaderF
"let g:Lf_ShortcutF = '<C-n>'
noremap <C-p> :LeaderfFunction!<CR>
"let g:Lf_ShortcutB = '<Leader>ln'
let g:Lf_StlSeparator = { 'left': '', 'right': '', 'font': '' }
let g:Lf_RootMarkers = ['.project', '.root', '.svn', '.git']
let g:Lf_WorkingDirectoryMode = 'Ac'
let g:Lf_WindowHeight = 0.30
let g:Lf_CacheDirectory = expand('~/.vim/cache')
let g:Lf_ShowRelativePath = 0
let g:Lf_HideHelp = 1
let g:Lf_StlColorscheme = 'powerline'
let g:Lf_PreviewResult = {'Function':0, 'BufTag':0}
"
""" == Airline
let g:airline_theme = 'bubblegum'
let g:airline_powerline_font = 1
let g:airline#extensions#tabline#enable = 1
"
""" == Rainbow
let g:rainbow_active = 1

